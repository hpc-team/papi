#!/bin/sh
set -e

commit=${1:-HEAD}

gitver=$(git describe ${commit})
gitver=${gitver#papi-}
tagver=${gitver%%-t-*}
gitver=${gitver#${tagver}-t-}
tagver=$(echo "$tagver" | tr - .)
snapver=${tagver}+git${gitver}
prefix=papi-${snapver}
tarxzball=${prefix}.tar.xz

git archive --format=tar --prefix=${prefix}/ ${commit} | xz -9 > ${tarxzball}

echo "Exported to ${tarxzball}"
